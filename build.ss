#!/usr/bin/scheme --libdirs .:nanopass

(parameterize ((optimize-level 3)
               (generate-wpo-files #t)
               (compile-imported-libraries #t)
               (debug-on-exception #t))
  (maybe-compile-file "nanopass/nanopass.ss")
  (maybe-compile-library "test.ss")
  (map (lambda (d)
         (if (and (not (member d '("examples"))) (file-directory? d))
           (map (lambda (t)
                  (if (and (>= (string-length t) 3)
                           (equal? ".ss" (substring t (- (string-length t) 3) (string-length t))))
                    (compile-script (string-append d "/test/" t))))
                (directory-list (string-append d "/test")))))
       (directory-list ".")))
