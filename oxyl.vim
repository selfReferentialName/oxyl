" VIM syntax file for oXyl

if exists("b:current_syntax")
	finish
endif

let b:current_syntax = "oxyl"

" syn iskeyword "@,A-Z,a-z,!,35-39,42-45,47-58,60-64,92,94-96,124,126"
setlocal iskeyword=33,36-38,42,43,45,47,48-57,60-64,@,91-94,_,123-126,-

" only includes normal initial keywords
syn keyword oxylInitialKeyword let impl if fun match rule monomorph let-param param

" only includes normal inner keywords
syn keyword oxylInnerKeyword in export then else of from where and or as mut when given lazy

" special initial keywords
syn keyword oxylRunKeyword run
syn keyword oxylNewTypeKeyword newsub newsuper newtype
syn keyword oxylMuKeyword mu
syn keyword oxylSyntaxKeyword syntax
syn keyword oxylModuleKeyword module import
syn keyword oxylTypeKeyword type class

" special unconditional inner keywords
syn keyword oxylRightArrowKeyword =>
syn match oxylBracketKeyword "[{}]"

" prefix operators
syn match oxylPrefixOperator '\.'

" comments
syn region oxylComment start="(\*" end="\*)" contains=oxylTodo,oxylComment
syn keyword oxylTodo contained TODO FIXME XXX NOTE

" numbers
syn match oxylDecimalNumber '-?[0-9]+(\.[0-9]*([eE]-?[0-9]+)?)?'
syn match oxylBinaryNumber '-?0[bB][0-1]+(\.[0-1]*([eE]-?[0-1]+)?)?'
syn match oxylOctalNumber '-?0[oO][0-7]+(\.[0-7]*([eE]-?[0-7]+)?)?'
syn match oxylHexNumber '-?0[xX][0-9a-fA-F]+(\.[0-9a-fA-F]*([eE]-?[0-9a-fA-F]+)?)?'

" symbol identifiers
" this breaks correct detection of word endings for some reason
" it also looks like it might summon cthulu
syn match oxylOperator '\<[@%\*/+\-~<!=>&|,:;\^\\\$][^ \s\(\)\[\],;]*\>'

" reserved identifiers
" oxylReservedBoth are used equally as types and values
" oxylReservedType are used for type, construction, and conversions
" oxylReservedValue are only used for values
" oxylReservedFlow is used for call/cc
syn keyword oxylReservedBoth top bot cont display nth dict fold
syn keyword oxylReservedType u8 u16 u32 u64 i8 i16 i32 i64 int uint bool array string ref ref! null eq ord com-monoid abelian com-ring field enum number integral monoid group ring dist-lattice bd-lattice boolean lattice functor applicative monad index mut-nth mut-dict thunk bigint com-monoid* abelian* monoid**
" syn match oxylReservedTypeMatch "\<(com-monoid\*\|abelian\*\|monoid\*\*)\>"
" hi def link oxylReservedTypeMatch Type
syn keyword oxylReservedValue id not true false copy from-u64 from-i64 from-u8 from-u16 from-u32 from-i8 from-i16 from-i32 inc dec pos? compl join meet map map-to map-apply pure set-nth! set-dict! foldt foldl foldr foldm find to-array map-to-array length any all slice slice-tail slice-head deref map2 zip some delay force id+ inv+ inv* id++ inv++ id+ id** id*
" syn match oxylReservedValueMatch "\<(id+\|inv+\|inv\*\|id++\|inv--\|id+\|id-\|id\*\*)\>"
" hi def link oxylReservedValueMatch Type
syn match oxylReservedFlow "call/cc"

" by default, all reserved stuff but call/cc is marked as a type
" this is because they all can be used similarly, and types are special while
" built in functions are not according to VIM syntax rules
" to override this behavior, hi def link oxylReserved*
" and let oxylReservedHi = 1
hi def link oxylReservedBoth  Type
hi def link oxylReservedType  Type
hi def link oxylReservedValue Type

hi def link oxylInitialKeyword    Keyword
hi def link oxylInnerKeyword      Keyword
hi def link oxylRunKeyword        PreProc
hi def link oxylNewTypeKeyword    Typedef
hi def link oxylMuKeyword         Keyword
hi def link oxylSyntaxKeyword     Macro
hi def link oxylModuleKeyword     Include
hi def link oxylTypeKeyword       Typedef
hi def link oxylRightArrowKeyword Keyword
hi def link oxylPrefixOperator    Operator
hi def link oxylComment           Comment
hi def link oxylTodo              Todo
hi def link oxylDecimalNumber     Number
hi def link oxylBinaryNumber      Number
hi def link oxylOctalNumber       Number
hi def link oxylHexNumber         Number
hi def link oxylOperator          Operator
if exists("oxylReservedHi") && oxylReservedHi
	hi def link oxylReserved Type
endif
hi def link oxylReservedFlow Exception
